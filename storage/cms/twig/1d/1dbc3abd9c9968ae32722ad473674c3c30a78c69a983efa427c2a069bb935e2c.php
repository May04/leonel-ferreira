<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/navigation.htm */
class __TwigTemplate_c0feeae84bc011b7dfb6423e6fc0e1f4807e6c782815689dae60ed8534a133b2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Nav -->
<nav id=\"nav\">
    <a href=\"";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\"><img src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo2.jpeg");
        echo "\" alt=\"Logo\" /></a>
    <ul>        
        <li><a href=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog");
        echo "\" class=\"";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 5), "id", [], "any", false, false, false, 5) == "blog")) {
            echo "active";
        }
        echo "\">Início</a></li>
        <li><a href=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contato");
        echo "\" class=\"";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 6), "id", [], "any", false, false, false, 6) == "contato")) {
            echo "active";
        }
        echo "\">Contato</a></li>
    </ul>
</nav>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/navigation.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 6,  48 => 5,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Nav -->
<nav id=\"nav\">
    <a href=\"{{ 'blog'|page }}\"><img src=\"{{'assets/images/logo2.jpeg'|theme}}\" alt=\"Logo\" /></a>
    <ul>        
        <li><a href=\"{{ 'blog'|page }}\" class=\"{% if this.page.id == 'blog' %}active{% endif %}\">Início</a></li>
        <li><a href=\"{{ 'contato'|page }}\" class=\"{% if this.page.id == 'contato' %}active{% endif %}\">Contato</a></li>
    </ul>
</nav>", "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/navigation.htm", "");
    }
}
