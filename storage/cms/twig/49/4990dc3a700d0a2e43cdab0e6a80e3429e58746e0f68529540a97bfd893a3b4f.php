<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/footer.htm */
class __TwigTemplate_f7a840ecd12233378e8167bdce1e34f0d7120bf14c4f1999153d361ae7ef40cc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Footer -->
<footer id=\"footer\">   
    <section>
        <h2>Contato</h2>
        <dl class=\"alt\">
            <dt>Endereço</dt>
            <dd>Rua Conselheiro Dantas, 2133, 701  
            <br>
            Caxias do Sul - RS - 95054-000, Brasil</dd>
            <dt>Telefone</dt>
            <dd>(54) 99104-8383</dd>
        </dl>
        <ul class=\"icons\">
            <li><a href=\"#\" class=\"icon fa-twitter alt\"><span class=\"label\">Twitter</span></a></li>
            <li><a href=\"#\" class=\"icon fa-facebook alt\"><span class=\"label\">Facebook</span></a></li>
            <li><a href=\"#\" class=\"icon fa-instagram alt\"><span class=\"label\">Instagram</span></a></li>
            <li><a href=\"#\" class=\"icon fa-whatsapp alt\"><span class=\"label\">Whatsapp</span></a></li>
            <li><a href=\"#\" class=\"icon fa-dribbble alt\"><span class=\"label\">Dribbble</span></a></li>
        </ul>        
    </section>
    <section>
        <h2>Atendimento 24 horas</h2>
       <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3483.9250282099774!2d-51.16436498967035!3d-29.166881464445698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951ebd03b2ae51ff%3A0x9e9b045920234357!2sLeonel%20Ferreira%20-%20Advogado!5e0!3m2!1spt-BR!2sbr!4v1584967077758!5m2!1spt-BR!2sbr\" width=\"auto\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
    </section>
    <p class=\"copyright\">&copy; 2020 Leonel Ferreira.</p> 
</footer>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Footer -->
<footer id=\"footer\">   
    <section>
        <h2>Contato</h2>
        <dl class=\"alt\">
            <dt>Endereço</dt>
            <dd>Rua Conselheiro Dantas, 2133, 701  
            <br>
            Caxias do Sul - RS - 95054-000, Brasil</dd>
            <dt>Telefone</dt>
            <dd>(54) 99104-8383</dd>
        </dl>
        <ul class=\"icons\">
            <li><a href=\"#\" class=\"icon fa-twitter alt\"><span class=\"label\">Twitter</span></a></li>
            <li><a href=\"#\" class=\"icon fa-facebook alt\"><span class=\"label\">Facebook</span></a></li>
            <li><a href=\"#\" class=\"icon fa-instagram alt\"><span class=\"label\">Instagram</span></a></li>
            <li><a href=\"#\" class=\"icon fa-whatsapp alt\"><span class=\"label\">Whatsapp</span></a></li>
            <li><a href=\"#\" class=\"icon fa-dribbble alt\"><span class=\"label\">Dribbble</span></a></li>
        </ul>        
    </section>
    <section>
        <h2>Atendimento 24 horas</h2>
       <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3483.9250282099774!2d-51.16436498967035!3d-29.166881464445698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951ebd03b2ae51ff%3A0x9e9b045920234357!2sLeonel%20Ferreira%20-%20Advogado!5e0!3m2!1spt-BR!2sbr!4v1584967077758!5m2!1spt-BR!2sbr\" width=\"auto\" height=\"300\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
    </section>
    <p class=\"copyright\">&copy; 2020 Leonel Ferreira.</p> 
</footer>", "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/footer.htm", "");
    }
}
