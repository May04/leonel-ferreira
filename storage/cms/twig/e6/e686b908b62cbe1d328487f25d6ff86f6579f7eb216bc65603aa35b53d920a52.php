<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/header.htm */
class __TwigTemplate_604951079cc0e247ccadc754eba9837d9d31f235ebf4b60c373ac59e50900b07 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Header -->
<header id=\"header\" class=\"alt\">
    <span class=\"logo\"><img src=\"";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/logo.jpeg");
        echo "\" alt=\"Logo\" /></span>
    <p>";
        // line 4
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 4), "description", [], "any", false, false, false, 4);
        echo "</p>
</header>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Header -->
<header id=\"header\" class=\"alt\">
    <span class=\"logo\"><img src=\"{{'assets/images/logo.jpeg'|theme}}\" alt=\"Logo\" /></span>
    <p>{{ this.page.description | raw }}</p>
</header>", "C:\\wamp64\\www\\leonel-ferreira/themes/zwiebl-zwiebl_stellar/partials/all_pages/header.htm", "");
    }
}
